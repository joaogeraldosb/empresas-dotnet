﻿using AutoMapper;
using Domain.Empresas.Entities;
using Service.Empresas.DTOs.Enterprises.Outputs;
using System;
using System.Collections.Generic;
using System.Text;

namespace Service.Empresas.MapperFactories
{
    public class ProfileToOutput : Profile
    {
        public ProfileToOutput()
        {
            CreateMap<EnterpriseType, EnterpriseTypeOutput>();
            CreateMap<Enterprise, EnterpriseDetailOutput>()
                .ForMember(dest => dest.CellPhone, opts => opts.MapFrom(src => src.Contact.CellPhone))
                .ForMember(dest => dest.ContactName, opts => opts.MapFrom(src => src.Contact.CellPhone))
                .ForMember(dest => dest.Email, opts => opts.MapFrom(src => src.Contact.CellPhone))
                .ForMember(dest => dest.Phone, opts => opts.MapFrom(src => src.Contact.CellPhone))
                .ForMember(dest => dest.EnterpriseTypeName, opts => opts.MapFrom(src => src.EnterpriseType.Name));
            CreateMap<Enterprise, EnterpriseIndexOutput>();
            ;
        }
    }
}
