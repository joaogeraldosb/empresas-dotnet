IF OBJECT_ID(N'[__EFMigrationsHistory]') IS NULL
BEGIN
    CREATE TABLE [__EFMigrationsHistory] (
        [MigrationId] nvarchar(150) NOT NULL,
        [ProductVersion] nvarchar(32) NOT NULL,
        CONSTRAINT [PK___EFMigrationsHistory] PRIMARY KEY ([MigrationId])
    );
END;

GO

CREATE TABLE [dbo].[EnterpriseTypes] (
    [Id] int NOT NULL IDENTITY,
    [Active] bit NOT NULL,
    [Name] varchar(50) NOT NULL,
    CONSTRAINT [PK_EnterpriseTypes] PRIMARY KEY ([Id])
);

GO

CREATE TABLE [dbo].[Users] (
    [Id] int NOT NULL IDENTITY,
    [Active] bit NOT NULL,
    [Name] varchar(100) NOT NULL,
    [Password] varbinary(max) NOT NULL,
    [LastLoginDate] datetime2 NULL,
    CONSTRAINT [PK_Users] PRIMARY KEY ([Id])
);

GO

CREATE TABLE [dbo].[Enterprises] (
    [Id] int NOT NULL IDENTITY,
    [Active] bit NOT NULL,
    [Name] varchar(100) NOT NULL,
    [Description] varchar(250) NOT NULL,
    [RegistrationDate] datetime NOT NULL,
    [IdEnterpriseType] int NOT NULL,
    [ContactName] varchar(100) NOT NULL,
    [Phone] varchar(15) NULL,
    [CellPhone] varchar(15) NOT NULL,
    [Email] varchar(100) NOT NULL,
    CONSTRAINT [PK_Enterprises] PRIMARY KEY ([Id]),
    CONSTRAINT [FK_Enterprises_EnterpriseTypes_IdEnterpriseType] FOREIGN KEY ([IdEnterpriseType]) REFERENCES [dbo].[EnterpriseTypes] ([Id]) ON DELETE NO ACTION
);

GO

CREATE TABLE [dbo].[ControlTokens] (
    [Id] bigint NOT NULL IDENTITY,
    [Active] bit NOT NULL,
    [Token] varchar(250) NOT NULL,
    [Client] varchar(50) NOT NULL,
    [SolicitationDate] datetime2 NOT NULL,
    [AuthenticationDate] datetime2 NULL,
    [IdUser] int NOT NULL,
    CONSTRAINT [PK_ControlTokens] PRIMARY KEY ([Id]),
    CONSTRAINT [FK_ControlTokens_Users_IdUser] FOREIGN KEY ([IdUser]) REFERENCES [dbo].[Users] ([Id]) ON DELETE NO ACTION
);

GO

IF EXISTS (SELECT * FROM [sys].[identity_columns] WHERE [name] IN (N'Id', N'Active', N'Name') AND [object_id] = OBJECT_ID(N'[dbo].[EnterpriseTypes]'))
    SET IDENTITY_INSERT [dbo].[EnterpriseTypes] ON;

IF EXISTS (SELECT * FROM [sys].[identity_columns] WHERE [name] IN (N'Id', N'Active', N'Name') AND [object_id] = OBJECT_ID(N'[dbo].[EnterpriseTypes]'))
    SET IDENTITY_INSERT [dbo].[EnterpriseTypes] OFF;

GO

IF EXISTS (SELECT * FROM [sys].[identity_columns] WHERE [name] IN (N'Id', N'Active', N'LastLoginDate', N'Name', N'Password') AND [object_id] = OBJECT_ID(N'[dbo].[Users]'))
    SET IDENTITY_INSERT [dbo].[Users] ON;
INSERT INTO [dbo].[Users] ([Id], [Active], [LastLoginDate], [Name], [Password])
VALUES (1, 1, NULL, 'testeapple@ioasys.com.br', 0xD76DF8D76DF8);
IF EXISTS (SELECT * FROM [sys].[identity_columns] WHERE [name] IN (N'Id', N'Active', N'LastLoginDate', N'Name', N'Password') AND [object_id] = OBJECT_ID(N'[dbo].[Users]'))
    SET IDENTITY_INSERT [dbo].[Users] OFF;

GO

CREATE INDEX [IX_ControlTokens_IdUser] ON [dbo].[ControlTokens] ([IdUser]);

GO

CREATE INDEX [IX_Enterprises_IdEnterpriseType] ON [dbo].[Enterprises] ([IdEnterpriseType]);

GO

INSERT INTO [__EFMigrationsHistory] ([MigrationId], [ProductVersion])
VALUES (N'20200217030025_InitialMigration', N'2.2.6-servicing-10079');

GO

