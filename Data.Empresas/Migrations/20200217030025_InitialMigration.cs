﻿using System;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Data.Empresas.Migrations
{
    public partial class InitialMigration : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.EnsureSchema(
                name: "dbo");

            migrationBuilder.CreateTable(
                name: "EnterpriseTypes",
                schema: "dbo",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Active = table.Column<bool>(nullable: false),
                    Name = table.Column<string>(type: "varchar(50)", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_EnterpriseTypes", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Users",
                schema: "dbo",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Active = table.Column<bool>(nullable: false),
                    Name = table.Column<string>(type: "varchar(100)", unicode: false, nullable: false),
                    Password = table.Column<byte[]>(nullable: false),
                    LastLoginDate = table.Column<DateTime>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Users", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Enterprises",
                schema: "dbo",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Active = table.Column<bool>(nullable: false),
                    Name = table.Column<string>(type: "varchar(100)", nullable: false),
                    Description = table.Column<string>(type: "varchar(250)", nullable: false),
                    RegistrationDate = table.Column<DateTime>(type: "datetime", nullable: false),
                    IdEnterpriseType = table.Column<int>(nullable: false),
                    ContactName = table.Column<string>(type: "varchar(100)", nullable: false),
                    Phone = table.Column<string>(type: "varchar(15)", nullable: true),
                    CellPhone = table.Column<string>(type: "varchar(15)", nullable: false),
                    Email = table.Column<string>(type: "varchar(100)", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Enterprises", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Enterprises_EnterpriseTypes_IdEnterpriseType",
                        column: x => x.IdEnterpriseType,
                        principalSchema: "dbo",
                        principalTable: "EnterpriseTypes",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "ControlTokens",
                schema: "dbo",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Active = table.Column<bool>(nullable: false),
                    Token = table.Column<string>(type: "varchar(250)", nullable: false),
                    Client = table.Column<string>(type: "varchar(50)", unicode: false, nullable: false),
                    SolicitationDate = table.Column<DateTime>(nullable: false),
                    AuthenticationDate = table.Column<DateTime>(nullable: true),
                    IdUser = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ControlTokens", x => x.Id);
                    table.ForeignKey(
                        name: "FK_ControlTokens_Users_IdUser",
                        column: x => x.IdUser,
                        principalSchema: "dbo",
                        principalTable: "Users",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.InsertData(
                schema: "dbo",
                table: "EnterpriseTypes",
                columns: new[] { "Id", "Active", "Name" },
                values: new object[,]
                {
                    { 1, true, "Agro" },
                    { 23, true, "Tourism" },
                    { 22, true, "Technology" },
                    { 21, true, "Software" },
                    { 20, true, "Social" },
                    { 19, true, "Smart City" },
                    { 18, true, "Service" },
                    { 17, true, "Real Estate" },
                    { 16, true, "Products" },
                    { 15, true, "Mining" },
                    { 14, true, "Media" },
                    { 24, true, "Transport" },
                    { 13, true, "Logistics" },
                    { 11, true, "Health" },
                    { 10, true, "Games" },
                    { 9, true, "Food" },
                    { 8, true, "Fintech" },
                    { 7, true, "Fashion" },
                    { 6, true, "Education" },
                    { 5, true, "Ecommerce" },
                    { 4, true, "Eco" },
                    { 3, true, "Biotech" },
                    { 2, true, "Aviation" },
                    { 12, true, "IOT" }
                });

            migrationBuilder.InsertData(
                schema: "dbo",
                table: "Users",
                columns: new[] { "Id", "Active", "LastLoginDate", "Name", "Password" },
                values: new object[] { 1, true, null, "testeapple@ioasys.com.br", new byte[] { 215, 109, 248, 215, 109, 248 } });

            migrationBuilder.CreateIndex(
                name: "IX_ControlTokens_IdUser",
                schema: "dbo",
                table: "ControlTokens",
                column: "IdUser");

            migrationBuilder.CreateIndex(
                name: "IX_Enterprises_IdEnterpriseType",
                schema: "dbo",
                table: "Enterprises",
                column: "IdEnterpriseType");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "ControlTokens",
                schema: "dbo");

            migrationBuilder.DropTable(
                name: "Enterprises",
                schema: "dbo");

            migrationBuilder.DropTable(
                name: "Users",
                schema: "dbo");

            migrationBuilder.DropTable(
                name: "EnterpriseTypes",
                schema: "dbo");
        }
    }
}
