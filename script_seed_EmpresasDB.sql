
INSERT INTO [dbo].[EnterpriseTypes] ([Id], [Active], [Name])
VALUES (1, 1, 'Agro'),
(23, 1, 'Tourism'),
(22, 1, 'Technology'),
(21, 1, 'Software'),
(20, 1, 'Social'),
(19, 1, 'Smart City'),
(18, 1, 'Service'),
(17, 1, 'Real Estate'),
(16, 1, 'Products'),
(15, 1, 'Mining'),
(14, 1, 'Media'),
(24, 1, 'Transport'),
(13, 1, 'Logistics'),
(11, 1, 'Health'),
(10, 1, 'Games'),
(9, 1, 'Food'),
(8, 1, 'Fintech'),
(7, 1, 'Fashion'),
(6, 1, 'Education'),
(5, 1, 'Ecommerce'),
(4, 1, 'Eco'),
(3, 1, 'Biotech'),
(2, 1, 'Aviation'),
(12, 1, 'IOT');

INSERT INTO [dbo].[Enterprises]
VALUES
(1, 'Contact Name 1','Description Enterprise 1 ',getdate(),1,'Enterprise 1','557930237777','5579999998888','enterprise1@test.com')
,(2,'Contact Name 2','Description Enterprise 2',getdate(), 2, 'Enterprise 2','557930237777','5579999998888','enterprise2@test.com')
,(3, 'Contact Name 3','Description Enterprise 1 ',getdate(),3,'Enterprise 1','557930237777','5579999998888','enterprise1@test.com')
,(4,'Contact Name 4','Description Enterprise 2',getdate(), 4, 'Enterprise 2','557930237777','5579999998888','enterprise2@test.com')
,(5, 'Contact Name 5','Description Enterprise 1 ',getdate(),5,'Enterprise 1','557930237777','5579999998888','enterprise1@test.com')
,(6,'Contact Name 6','Description Enterprise 2',getdate(), 6, 'Enterprise 2','557930237777','5579999998888','enterprise2@test.com')
,(7, 'Contact Name 7','Description Enterprise 1 ',getdate(),7,'Enterprise 1','557930237777','5579999998888','enterprise1@test.com')
,(8,'Contact Name 8','Description Enterprise 2',getdate(), 8, 'Enterprise 2','557930237777','5579999998888','enterprise2@test.com')
,(9, 'Contact Name 9','Description Enterprise 1 ',getdate(),9,'Enterprise 1','557930237777','5579999998888','enterprise1@test.com')
,(10,'Contact Name 10','Description Enterprise 2',getdate(), 10, 'Enterprise 2','557930237777','5579999998888','enterprise2@test.com')
GO
	